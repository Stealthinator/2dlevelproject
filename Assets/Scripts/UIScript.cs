using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIScript : MonoBehaviour
{
    public TextMeshProUGUI coinCounter;
    public TextMeshProUGUI hpCounter;
    public TextMeshProUGUI loseText;
    public PlayerMovement playerInfo;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        coinCounter.text = "Coins: " + playerInfo.coins;
        hpCounter.text = "HP: " + playerInfo.hitpoints;
        if(playerInfo.hitpoints <= 0)
        {
            loseText.text = "YOU LOSE!";
        }
        if(playerInfo.coins >= 10)
        {
            loseText.text = "YOU WON!";
        }
    }
}
