using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{



    public Rigidbody2D rb;
    public float moveSpeed = 30;
    public float jumpForce = 600;
    public Transform groundPoint;
    public LayerMask groundLayer;
    private bool isOnGround;
    public Animator animator;
    public int coins = 0;
    public int hitpoints = 3;
    private float inputX;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(inputX * moveSpeed, rb.velocity.y);

        isOnGround = Physics2D.OverlapCircle(groundPoint.position, .2f, groundLayer);

        animator.SetFloat("moveSpeed", Mathf.Abs(rb.velocity.x));
        animator.SetBool("IsOnGround", isOnGround);

        if(rb.velocity.x > 0f)
        {
            transform.localScale = Vector3.one;
        }
        else if (rb.velocity.x < 0f)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
       if(hitpoints <= 0)
       {
           Destroy(gameObject);
       }
    }

    public void Move(InputAction.CallbackContext context)
    {
        inputX = context.ReadValue<Vector2>().x;
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if(context.performed && isOnGround)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
    }

    private void OnTriggerEnter2D(Collider2D Other)
    {
        if(Other.gameObject.CompareTag("Coin"))
        {
            coins += 1;
            Destroy(Other.gameObject);
        }
        
    }

}
