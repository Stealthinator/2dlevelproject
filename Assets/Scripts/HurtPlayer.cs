using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayer : MonoBehaviour
{
    public PlayerMovement playerInfo;
    
    private void OnTriggerEnter2D(Collider2D Other)
    {
        if(Other.gameObject.CompareTag("Player"))
        {
            playerInfo.hitpoints -= 1;
            //Destroy(gameObject);
        }
    }
}
