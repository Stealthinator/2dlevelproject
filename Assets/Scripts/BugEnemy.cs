using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugEnemy : MonoBehaviour
{
    public Transform Player = null;
    
    public Rigidbody2D rb;


    // Update is called once per frame
    void Update()
    {
        if(Player != null)
        {
            if(transform.position.x < Player.position.x)
            {
                rb.velocity = new Vector2(5, rb.velocity.y);
            }
            else if(transform.position.x > Player.position.x)
            {
                rb.velocity = new Vector2(-5, rb.velocity.y);
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D Other)
    {
        if(Other.gameObject.CompareTag("Player"))
        {
            Player = Other.transform;
        }
    }
    
    private void OnTriggerExit2D(Collider2D Other)
    {
        if(Other.gameObject.CompareTag("Player"))
        {
            Player = null;
        }
    }
}
